﻿-- Exported from QuickDBD: https://www.quickdatatabasediagrams.com/
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.

DROP SCHEMA zavrsnidb;

CREATE SCHEMA zavrsnidb;

USE zavrsnidb;


CREATE TABLE `VrijemePolaska` (
    `IdVremenaPolaska` int  NOT NULL ,
    `Vrijeme` time  NOT NULL ,
    PRIMARY KEY (
        `IdVremenaPolaska`
    )
);

CREATE TABLE `Ruta` (
    `IdRute` int  NOT NULL ,
    `NazivRute` varchar(100)  NOT NULL ,
    `Polaziste` varchar(30)  NOT NULL ,
    `Odrediste` varchar(30)  NOT NULL ,
    `BrzinaRute` double  NOT NULL ,
    PRIMARY KEY (
        `IdRute`
    )
);

CREATE TABLE `Koordinata` (
    `IdKoordinate` int  NOT NULL ,
    `GeografskaSirina` double  NOT NULL ,
    `GeografskaDuljina` double  NOT NULL ,
    PRIMARY KEY (
        `IdKoordinate`
    )
);

CREATE TABLE `Stanica` (
    `IdStanice` int  NOT NULL ,
    `NazivStanice` varchar(30)  NOT NULL ,
    `GeografskaSirinaStanice` double  NOT NULL ,
    `GeografskaDuljinaStanice` double  NOT NULL ,
    PRIMARY KEY (
        `IdStanice`
    )
);

CREATE TABLE `VrijemePolaskaRute` (
    `IdVremenaPolaska` int  NOT NULL ,
    `IdRute` int  NOT NULL ,
    PRIMARY KEY (
        `IdVremenaPolaska`,`IdRute`
    )
);

CREATE TABLE `RutaStanica` (
    `IdRute` int  NOT NULL ,
    `IdStanice` int  NOT NULL ,
    PRIMARY KEY (
        `IdRute`,`IdStanice`
    )
);

CREATE TABLE `KoordinateRute` (
    `IdKoordinate` int  NOT NULL ,
    `IdRute` int  NOT NULL ,
    PRIMARY KEY (
        `IdKoordinate`,`IdRute`
    )
);

ALTER TABLE `VrijemePolaskaRute` ADD CONSTRAINT `fk_VrijemePolaskaRute_IdVremenaPolaska` FOREIGN KEY(`IdVremenaPolaska`)
REFERENCES `VrijemePolaska` (`IdVremenaPolaska`);

ALTER TABLE `VrijemePolaskaRute` ADD CONSTRAINT `fk_VrijemePolaskaRute_IdRute` FOREIGN KEY(`IdRute`)
REFERENCES `Ruta` (`IdRute`);

ALTER TABLE `RutaStanica` ADD CONSTRAINT `fk_RutaStanica_IdRute` FOREIGN KEY(`IdRute`)
REFERENCES `Ruta` (`IdRute`);

ALTER TABLE `RutaStanica` ADD CONSTRAINT `fk_RutaStanica_IdStanice` FOREIGN KEY(`IdStanice`)
REFERENCES `Stanica` (`IdStanice`);

ALTER TABLE `KoordinateRute` ADD CONSTRAINT `fk_KoordinateRute_IdKoordinate` FOREIGN KEY(`IdKoordinate`)
REFERENCES `Koordinata` (`IdKoordinate`);

ALTER TABLE `KoordinateRute` ADD CONSTRAINT `fk_KoordinateRute_IdRute` FOREIGN KEY(`IdRute`)
REFERENCES `Ruta` (`IdRute`);

GRANT ALL ON zavrsnidb.* TO 'testuser'@'localhost';

